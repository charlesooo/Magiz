![Title](https://images.gitee.com/uploads/images/2020/0229/190333_89cb445e_5749239.gif "06.gif")
# Magiz v0.4.0 
[![star](https://gitee.com/zhou_shi_hu/Magiz/badge/star.svg?theme=dark)](https://gitee.com/zhou_shi_hu/Magiz/stargazers) [![fork](https://gitee.com/zhou_shi_hu/Magiz/badge/fork.svg?theme=dark)](https://gitee.com/zhou_shi_hu/Magiz/members)   
SketchUp插件，用于处理大量体块的建筑造型，可定义，可拓展。（[查看演示](https://gitee.com/zhou_shi_hu/Magiz/blob/master/Magiz_Demo.gif)）   
***
#### 基本操作
![任务栏图标](https://images.gitee.com/uploads/images/2020/0229/140445_1b70bdf7_5749239.png "ToolBar.png")  
1.  选中一个或多个群组或组件
2. 通过任务栏图标进行操作，四个按钮从左至右分别代表：有细节的变形、无细节的变形、重置体块、样式选择器
3. 样式选择器关闭时，变形均为随机变形。打开样式选择器并选中一个样式以后，变形均为按指定样式变形
***
#### 运行机制  

1. 随机变形会根据是否是群组，是否超过24m，在四种类型下进行随机样式选择  

    | -| h<24m | h>24m|
    | --- | ------ | --- |
    | 群组 - Group | 多层公建 - TierBuilding | 高层公建 - Skyscraper |
    | 组件 - Component | 多层住宅 - Villa | 高层住宅 - Apartment |

2. 变形样式可以通过编辑Resource文件夹中的mgz文件进行自定义和拓展。新加入的mgz文件在每次SketchUp启动时均会自动载入样式管理器。Resource文件夹一般在`C:\Users\Administrator\AppData\Roaming\SketchUp\SketchUp 2018\SketchUp\Plugins\Zhouxi_Magiz\Resource`
3. mgz文件为Json格式。文件结构说明以及创建和编辑教程参见[WIKI](https://gitee.com/zhou_shi_hu/Magiz/wikis/)
***
#### 安装教程

1.  下载Zhouxi_Magiz_v0..4.0.rbz
2.  SketchUp2017版本及以上，窗口(Window) ---> 插件管理器(Extension Manager) ---> 安装
***
#### 更新支持

![QRCode](https://images.gitee.com/uploads/images/2020/0229/184541_1ee6fd3e_5749239.png "QRcode.png")   
微信公众号：Catching Up    
邮箱：zhou.xiii@qq.com  
***